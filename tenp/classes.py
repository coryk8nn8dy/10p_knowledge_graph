################################################################################
# classes.py                                                                   #
#                                                                              #
# This file contains the class definitions for the 10p knowledge graph         #
# application                                                                  #
#                                                                              #
# Author : Cory Kennedy                                                        #
# Date Created : 28 July 2020                                                  #
# Date Last Modified : 28 July 2020                                            #
################################################################################

import abc


################################################################################
# Node class definitions                                                       #
#                                                                              #
# Nodes in the 10p knowledge graph consist of 'Static' class instances.        #
# 'Static' is inherited by 'Position' and 'Submission' classes.                #
################################################################################

class Static(abc.ABC):
    
    @abc.abstractproperty
    def name(self):
        raise NotImplementedError

    @abc.abstractproperty
    def precedence(self):
        raise NotImplementedError


class Position(Static):
    
    name = ''
    
    precedence = 0

    def __init__(self, name, precedence):

        self.name = name

        self.precedence = precedence        


class Submission(Static):
    
    name = ''
    
    precedence = 0

    def __init__(self, name, precedence):

        self.name = name

        self.precedence = precedence        
