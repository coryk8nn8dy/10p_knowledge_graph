################################################################################
# context.py                                                                   #
#                                                                              #
# This file provides context for the test suite by allowing it path access to  #
# the 10p knowledge graph application                                          #
#                                                                              #
# Author : Cory Kennedy                                                        #
# Date Created : 29 July 2020                                                  #
# Date Last Modified : 29 July 2020                                            #
################################################################################
from sys import path as spath
from os import path as opath
spath.insert(0, opath.abspath(opath.join(opath.dirname(__file__), '..')))

import tenp
    
