################################################################################
# test_classes.py                                                              #
#                                                                              #
# This file contains test for the all of the class definitions for the 10p     #
# knowledge graph application                                                  #
#                                                                              #
# Author : Cory Kennedy                                                        #
# Date Created : 29 July 2020                                                  #
# Date Last Modified : 29 July 2020                                            #
################################################################################

import unittest
from context import tenp
from tenp import classes


################################################################################
# Node class definitions                                                       #
#                                                                              #
# Nodes in the 10p knowledge graph consist of statics. Statics are further     #
# broken down into positions and submissions.                                  #
################################################################################

# Static is an abstract parent class of Position and Submission 
class TestStatic(unittest.TestCase):

    # test that the constructor calls fail because 'Static' is an abstract class
    def test_static(self):
        self.assertRaises(Exception,
                          lambda:tenp.classes.Static(test_name, test_precedence),
                          "Should throw a NotImplementedError")

    # test that the subclasses of 'Static' are instantiable
    def test_static_subclasses(self):
        test_pos_obj = tenp.classes.Position('name', 'precedence')
        test_sub_obj = tenp.classes.Submission('name', 'precedence')
        self.assertIsInstance(test_pos_obj, tenp.classes.Position,
                              "'test_pos_obj' should be a 'Submission' instance")
        self.assertIsInstance(test_sub_obj, tenp.classes.Submission,
                              "'test_sub_obj' should be a 'Position' instance")


################################################################################
# MAIN                                                                         #
################################################################################

if __name__ == '__main__':
    unittest.main()
